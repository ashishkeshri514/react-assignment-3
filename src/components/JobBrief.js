import React, { Component } from "react";
import Loader from "./Loader";
import "./JobBrief.css"
export class JobBrief extends Component {
  state = {
    name: "",
    location: "",
    logo: "",
    description: "",
    salary: "",
    city: "",
    loading: false,
  };

  componentDidMount() {
    let { name, location, logo, description, salaryInString } = this.props;
    this.setState({
      name,
      location,
      logo,
      description,
      salaryInString,
    });
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false });
    }, 5000);
  }
  onClickJob = (event) => {
    this.props.onClick(this.state);
  };
  render() {
    return (
      <div className="jobs">
        {this.state.loading && <Loader />}
        {!this.state.loading && (
          <div
            className="job-brief"
            onClick={this.onClickJob}
          >
            <img alt="logo" src={this.props.logo} className="jobs img" />
            <div className="jobDetails">
            <div style={{ fontWeight: "500", fontSize: "20px" }}>{this.props.name}</div>
            <div style={{ fontSize: "15px" }}>
              {this.props.location?.city},{this.props.location?.country}
            </div>
            
            <p style={{
                    marginTop: "5px",
                    marginBottom: "5px",
                    fontSize: "13px",
                    lineHeight: "1em",
                    maxHeight: "3em",
                    overflow: "hidden",
                  }}>{this.props.description}</p>
            <div>
                  <span style={{ fontWeight: "500" }}>Salary:</span>{" "}{this.props.salaryInString}</div>

                  <div className="btns">
                  <button className="btn_Apply">Apply</button>
                  <button className="btn_Cancel">Not Intrested</button>
                </div>
          </div>
          </div>
        )}
      </div>
    );
  }
}

export default JobBrief;
