import React, { Component } from "react";
import Education from "./Education";
import PersonalInformation from "./PersonalInformation";
import Skills from "./Skills";
import WorkExperience from "./WorkExperience";

export class Profile extends Component {
  render() {
    return (
      <div>
        <div
                      className="four wide column"
                      style={{ margin: "auto" }}
                    >
                      <a className="ui button primary" href="/">
                        Home
                      </a>
                    </div>
      <div style={{ margin:10,backgroundColor:"white",fontSize: "90%", padding:'5vw',borderRadius: "2vw" }}>
        
        <PersonalInformation />
        <br />
        <WorkExperience />
        <br />
        <Education />
        <br />
        <Skills />
        <br />
        
      </div>
      </div>
    );
  }
}

export default Profile;
