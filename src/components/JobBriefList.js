import React, { Component } from "react";
import JobBrief from "./JobBrief";
import JobDetails from "./JobDetails";
import Loader from "./Loader";
import "./JobBrief.css"
export class JobBriefList extends Component {
  constructor(props) {
    super(props);

    this.state = { selectedJob: null, loading: false, jobs: [] };
  }
  jobselect = (e) => {
    this.setState({ selectedJob: e });
  };

  componentDidMount() {
    this.setState({
      loading: true,
      jobs: this.props.jobList,
      selectedJob: null,
    });

    setTimeout(() => {
      this.setState({ loading: false });
    }, 5000);
  }
 
  render() {
    return (
      <div
        style={{
          display: "flex",
          alignItems: "flex-start",
          marginTop: "5px",
          overflow: "hidden",
          padding: '1vw'
        }}
      >
        <div
          className="jobsView" style={{ margin: "auto",
          width: "180vw",
          border: "1px solid gray",
          maxHeight: "93vh",
          overflow: "auto",
          borderRadius: "4px",
          padding:"1vw",
}}
        > 
        
          {this.state.loading && <Loader />}
          {!this.state.loading &&
            this.props.jobList.map((job, index) => (
              <JobBrief
                key={index}
                name={job.name}
                salary={job.salary}
                salaryInString={job.salary}
                description={job.description}
                logo={job.logo}
                location={job.location}
                onClick={this.jobselect}
              /> 
            ))}
        
        </div>

        <div style={{ display:"flex",width: "450vw",borderRadius: "2vw",height:"35vw",padding:"3vw", margin: "100 100 0 10", backgroundColor:"white",fontSize: "90%" }}>
          {this.state.selectedJob && (
            <JobDetails job={this.state.selectedJob} />
          )}
        </div>
      </div>
    );
  }
}

export default JobBriefList;
