import React, { Component } from "react";

export default class JobDetails extends Component {
  render() {
    return (
      <div style={{ textAlign: "left", border: "1px solid gray",padding:"2vh" }}>
        <div style={{ textAlign: "center"}}>
        <h1> <b>{this.props.job?.name}</b></h1>
        <h2> <b>{this.props.job?.location?.city}</b></h2></div>
        <img alt="logo" src={this.props.job?.logo} className="job-post-image" /><br/><br/>
        <div className="btns">
                  <button className="btn_Apply">Apply</button>
        </div><br/>
        <i>{this.props.job?.description}</i><br/><br/>
        <b>Post Name:</b> <i>{this.props.job?.name}</i><br/>
        <b>No. of Post:</b> <i>2</i><br/>
        <b>Salary:</b> <i>{this.props.job?.salaryInString}</i><br/>
        <b>Location:</b> <i>{this.props.job?.location?.city}</i><br/>
        
        

      </div>
    );
  }
}

